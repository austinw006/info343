var services = {
	hardware: [
	{
		description: 'PC Repair, Service &amp; Support',
		price: ''
	}, 
	{
		description: 'Custom Built Computer Systems',
		price: ''
	}, 
	{
		description: 'PC Components &amp; Parts/Hardware Sales',
		price: ''
	}, 
	{
		description: 'Re-formatting &amp; Reinstalling Software',
		price: ''
	},
	{
		description: 'Recovering Lost Hardware Data',
		price: ''
	},
	{
		description: 'Hard Drive &amp; Data Backups',
		price: ''
	},
	{
		description: 'Laptop Sales/Repair Service/Help',
		price: ''
	},
	{
		description: 'Virus Scanning &amp; Removal',
		price: ''
	},
	{
		description: 'Computer Upgrades/Hardware &amp; Software Configuration',
		price: ''
	},
	{
		description: 'Windows Operating System Installations/Sales',
		price: ''
	},
	{
		description: 'PC Help &amp; Technical Support',
		price: ''
	},
	{
		description: 'Troubleshooting &amp; Diagnosis',
		price: ''
	},
	{
		description: 'System Crash Rescue',
		price: ''
	},
	{
		description: 'Internet Security/Access Help',
		price: ''
	},
	{
		description: 'Residential/Business Computer Repair',
		price: ''
	},
	{
		description: 'Network Cabling &amp; Wiring/Wireless Networking',
		price: ''
	},
	{
		description: 'Computer Cleaning &amp; Preventive Maintenance',
		price: ''
	}

	], 
	software: [
	
	]
};

$(document).ready(function(){
	getElements();
	render();
});

function getElements() {
	services.template = $('#services_template').html();
	services.$container = $('.services');
};

function render() {
	var templated_data = _.template(services.template, services);
	services.$container.html(templated_data);
};